import { Account } from './../../app/models/account';
import { Injectable } from '@angular/core';
import { Http, Response, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable() // To allow this class to be called by other component as a service 
export class UserServicesProvider {
  constructor(private http: Http) { } // contruct http features

  postUser(user: Account): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' }); // set json as Content-Type http header
    let options = new RequestOptions({ headers: headers }); // encapsulate header into http request options
    let body = JSON.stringify(user); // convert user Account object into JSON format

    return this.http.post('http://127.0.0.1:3000/api/accounts', body, options) // post / submit user Account json to Loopback API
      .map(
      (res: Response) => res.json() // if there is a response, convert it into json
      )
      .catch(
      error => Observable.throw(error) // thow if there is an error
      )
  }

  getUsers(): Observable<any[]> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.get('http://127.0.0.1:3000/api/accounts', options)
      .map(
      (res: Response) => res.json()
      )
      .catch(
      error => Observable.throw(error)
      )
  }

  deleteUser(user: Account): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });

    return this.http.delete('http://127.0.0.1:3000/api/accounts/'+ user.id, options)
      .map(
      (res: Response) => res.json()
      )
      .catch(
      error => Observable.throw(error)
      )
  }

}
