import { UserServicesProvider } from './../../providers/user-services/user-services';
import { Account } from './../../app/models/account';
import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {

  public user: Account = new Account(); // create instance user from Account class as data container for posting the form
  public users: Account[] = new Array(); // create array instance as data container to retrieve list of user account

  constructor(
    public navCtrl: NavController,
    public usersvc: UserServicesProvider, // contruct UserServicesProvicer so that it can be used to get / post data through it
    private alertCtrl: AlertController // contruct AlertCOntroller to be used at alert box
  ) {
  }

  ngOnInit() { // All commands will be executed inside here before page rendering
    setTimeout(
      () => {
        this.getUsers()
      }, 100
    )
  }

  postUser() {
    this.usersvc.postUser(this.user) // calling method from Account Service Provider
      .subscribe( // posting the data through subscribing postUser service
      data => data, // return data from post operation if any
      error => console.log(error) // print any error if its return any problem
      )
    this.navCtrl.setRoot(HomePage); // reload/ refresh the same page by self navigation
  }

  getUsers() { // this function will be called before rendering page via ngOnInit to 
    this.usersvc.getUsers() // calling method from Account Service Provider
      .subscribe( // get the data though subscribing getUsers service
      data => {
        this.users = data as Account[]; // casting data type to Account class
      },
      error => console.log(error) // print any error if its return any problem
      )
  }

  presentConfirm(usr: Account) {
    let alert = this.alertCtrl.create({ // creating alert dialogbox for deletation confirmation
      title: 'Confirm deletion',
      message: 'Are you sure want to delete ' + usr.name + ' account?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.usersvc.deleteUser(usr)
              .subscribe();
            this.navCtrl.setRoot(HomePage);
          }
        }
      ]
    });
    alert.present();
  }
}
